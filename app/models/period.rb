class Period < ActiveRecord::Base
  has_many :events, through: :event_job_period_associations
  has_many :jobs, through: :event_job_period_associations
  has_many :event_job_period_associations, dependent: :destroy
end
